
/**
 * \defgroup group_edgeai_cpp_apps TI EdgeAI C++ applications
 *
 * \brief TI Edge AI C++ applications
 *
 * \ingroup group_edgeai_api
 */

/**
 * \defgroup group_dl_inferer Deep Learning Inference engine
 *
 * \brief Unified interface for running different DL run time APIs
 *
 * \ingroup group_edgeai_api
 */
